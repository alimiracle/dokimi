# dokimi
CPU Benchmark and stress

## Introduction:
A short time ago I bought a new computer
And I wanted to make a comparison of the speed of the processor between the new computer and the old laptop.
So I wrote this app.

## Build Requirements:
- c Compiler like gcc
- gnu make
- gengetopt
## Build:
Like most free software aps

Open the terminal and go to the source code, and then type make

to Install the app:
type make install as root

## Usage:
Without parameters:

```
$ ./dokimi
```

by default it will use all CPU cores and Executes 9000000000 iteration

you can specifying the number of CPU cors useing -w or --workers

```
$ ./dokimi -w 3
```

it will use 3 CPU cores and Executes 9000000000 iteration

and you can specifying the number of iterations useing -i or --iterations

```
$ ./dokimi -i 40000
```

it will use all CPU cores and Executes 40000 iteration

by default dokimi Depends on a random number during the square root operation

you can Specify the number used in the square root operation useing -n or --number_sqrt

```
$ ./dokimi -n 40000
```

## FAQ:
Is it portable?

dokimi is designed to work in POSIX systems

What does dokimi name Means?

dokimi is Greek word means test in English
## Trouble:
All efforts have been made to ensure the smooth and correct running of this application. If you find dokimi is behaving abnormaly though, there are 3 options you can try:

1) Turn it off and run away. Not an option I would advise.

2) Write a harsh comment that says how this app is a pile of **** and you can't believe I even dared to waste your time.. Again, not a great option, but it does make me laugh when I read some of the stuff.. :-)

3) Send me a short email with error type and any other information you think is relevant, and I'll fix it. Jackpot.

When I find a bug, I crush it. If I don't find it, and you do, and don't tell me, it lives and we all lose.. I'm not a mind-reader. Or a Computer-reader. I'm not one of the X-Men. i'm alimiracle
## license:
gpl v3
