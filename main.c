/* A program to make  CPU Benchmark and Stress.

 * Copyright (C) 2022
 * Alimiracle <alimiracle@riseup.net>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

    #include <ctype.h>
#include <signal.h>
    #include <math.h>

    #include <stdio.h>

    #include <stdlib.h>
#include <unistd.h>
    #include <time.h>

    #include <sys/wait.h>

    #include <sys/time.h>

    #include "cmdline.h"

    static char * global_progname = "dokimi";

    #define out fprintf(stdout, "%s: info: ", global_progname), \
      fprintf
    #define err fprintf(stderr, "%s error: ", global_progname), \
      fprintf
      long long count = 0;

    int
    timeval_subtract(result, x, y)
    struct timeval *
      result, * x, * y;

    {

      /* Perform the carry for the later subtraction by updating y. */
      if (x -> tv_usec < y -> tv_usec)

      {

        int nsec = (y -> tv_usec - x -> tv_usec) / 1000000 + 1;

        y -> tv_usec -= 1000000 * nsec;

        y -> tv_sec += nsec;

      }

      if (x -> tv_usec - y -> tv_usec > 1000000)

      {

        int nsec = (x -> tv_usec - y -> tv_usec) / 1000000;

        y -> tv_usec += 1000000 * nsec;

        y -> tv_sec -= nsec;

      }

      /* Compute the time remaining to wait.
         tv_usec is certainly positive. */
      result -> tv_sec = x -> tv_sec - y -> tv_sec;

      result -> tv_usec = x -> tv_usec - y -> tv_usec;

      /* Return 1 if result is negative. */
      return x -> tv_sec < y -> tv_sec;

    }

    void
    test_pid(pid_t pid) {
      if (getppid() != pid)

      {
        exit(0);

      }

    }

void sig_handler(int signum){
printf("%lld\n", count);

exit(0);

}


    int do_task(long long n, long sqrt_n, int time_s) {
if(time_s!=0) {
alarm(time_s);
 signal(SIGALRM,sig_handler); // Register signal handler
      n=-6;
}
      int test_pid_wait = 0;

      pid_t pid = getppid();

      int i;

      double d;

      float f;


      while (count != n) {

        d = sqrt(sqrt_n);
        f = sqrt(sqrt_n);

        i = sqrt(sqrt_n);

        if (test_pid_wait == 100) {
          test_pid(pid);

          test_pid_wait = 0;
        }

        test_pid_wait = test_pid_wait + 1;
        count = count + 1;
      }

      return 1;
    }


// Come with soft rounded cheeks and eyes as bright As sunlight on a stream;

    int main(int argc, char ** argv) {
      struct gengetopt_args_info args_info;

      if (cmdline_parser(argc, argv, & args_info) != 0)
        exit(1);

      char * p;

      int number_of_workers = args_info.workers_arg;

      long number_of_iterations = args_info.iterations_arg;
      long number_of_sqrt = args_info.number_sqrt_arg;

      int i = 0;

      int status = 0;
int time_s=0;
      if (number_of_workers == 0) number_of_workers = sysconf(_SC_NPROCESSORS_ONLN);
      if (number_of_sqrt == 0) number_of_sqrt = rand();
if(args_info.strikes_flag!=0) time_s = args_info.time_arg;

      /* Record our start time.  */

      unsigned long int elapsed;
      struct timeval t_start, t_end, t_diff;

      gettimeofday( & t_start, NULL);

      fprintf(stdout, "Creating %d children\n", number_of_workers);

      fflush(stdout);

      for (i = 0; i < number_of_workers; i++) {
        pid_t pid = fork();

        if (pid == 0) /* only execute this if child */ {

          do_task(number_of_iterations, number_of_sqrt, time_s);

          exit(0);
        }

      }

      //              wait(&status);  /* only the parent waits */
      while (wait(NULL) > 0);

      /* Record our stop time.  */

      gettimeofday( & t_end, NULL);

      timeval_subtract( & t_diff, & t_end, & t_start);

      elapsed = (t_diff.tv_sec * 1e6 + t_diff.tv_usec);

      /* Print final status message.  */
if(args_info.strikes_flag==0) fprintf(stdout, "successful run completed in %lu MS\n",
        elapsed);

      return 0;
    }
