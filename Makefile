dokimi: main.c
	gengetopt -i dokimi.ggo
	sed -i "s/long iterations_arg/long long iterations_arg/g" cmdline.h
	cc main.c cmdline.c -lm --static -o dokimi
install:
	cp dokimi /usr/bin/
clene:
	rm /usr/bin/dokimi
